﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Xml.Linq;
using Microsoft.Office.Tools.Excel;
using Microsoft.VisualStudio.Tools.Applications.Runtime;
using Excel = Microsoft.Office.Interop.Excel;
using Office = Microsoft.Office.Core;
using System.ServiceModel;

namespace PatientConversionTool
{

    public partial class Sheet1
    {

        private void Sheet1_Startup(object sender, System.EventArgs e)
        {
        }

        private void Sheet1_Shutdown(object sender, System.EventArgs e)
        {
        }

        #region VSTO Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InternalStartup()
        {
            this.RunButton.Click += new System.EventHandler(this.RunButton_Click);
            this.CancelButton.Click += new System.EventHandler(this.CancelButton_Click);
            this.backgroundWorker1.DoWork += new System.ComponentModel.DoWorkEventHandler(this.backgroundWorker1_DoWork);
            this.Startup += new System.EventHandler(this.Sheet1_Startup);
            this.Shutdown += new System.EventHandler(this.Sheet1_Shutdown);

        }

        #endregion

        private void backgroundWorker1_DoWork(object sender, System.ComponentModel.DoWorkEventArgs e)
        {
            //setting up the excel sheet with appropriate variables
            var misValue = System.Reflection.Missing.Value;
            var excel = Globals.ThisWorkbook.Application;
            var activeSheet = excel.ActiveSheet;
            int n = Int32.Parse(nStartRow.Text.ToString());
            string ActiveRow = excel.ActiveSheet.Range["A" + n, misValue].text;
            

            //while statement to run through everything

            while (ActiveRow.ToString().Length > 0)
            {
                ActiveRow = excel.ActiveSheet.Range["A" + n, misValue].text;
                if (ActiveRow == "")
                {
                    break;
                }
                
                //check for cancellation
                if (this.backgroundWorker1.CancellationPending)
                {
                    e.Cancel = true;
                    return;
                }

                #region Establish Variables
                //Establish Variables
                string RecordID = activeSheet.Range["A" + n, misValue].text;
                string BranchOffice = activeSheet.Range["B" + n, misValue].text;
                string LastName = activeSheet.Range["C" + n, misValue].text;
                string FirstName = activeSheet.Range["D" + n, misValue].text;
                string MiddleName = activeSheet.Range["E" + n, misValue].text;
                string Suffix = activeSheet.Range["F" + n, misValue].text;
                string AccountNumber = activeSheet.Range["G" + n, misValue].text;
                string AccountOnHold = activeSheet.Range["H" + n, misValue].text;
                string BillingAddress1 = activeSheet.Range["I" + n, misValue].text;
                string BillingAddress2 = activeSheet.Range["J" + n, misValue].text;
                string City = activeSheet.Range["K" + n, misValue].text;
                string State = activeSheet.Range["L" + n, misValue].text;
                string PostalCode = activeSheet.Range["M" + n, misValue].text;
                string BillingPhone = activeSheet.Range["N" + n, misValue].text;
                string MobilePhone = activeSheet.Range["O" + n, misValue].text;
                string Email = activeSheet.Range["P" + n, misValue].text;
                string DelivAddress1 = activeSheet.Range["Q" + n, misValue].text;
                string DelivAddress2 = activeSheet.Range["R" + n, misValue].text;
                string DelivCity = activeSheet.Range["S" + n, misValue].text;
                string DelivState = activeSheet.Range["T" + n, misValue].text;
                string DelivPostalCode = activeSheet.Range["U" + n, misValue].text;
                string DelivPhoneNumber = activeSheet.Range["V" + n, misValue].text;
                
                //Patient General Info
                string DOB = activeSheet.Range["W" + n, misValue].text;
                string SSN = activeSheet.Range["X" + n, misValue].text;
                string TaxZone = activeSheet.Range["Y" + n, misValue].text;
                string User1 = activeSheet.Range["Z" + n, misValue].text;
                string User2 = activeSheet.Range["AA" + n, misValue].text;
                string User3 = activeSheet.Range["AB" + n, misValue].text;
                string User4 = activeSheet.Range["AC" + n, misValue].text;
                string Height = activeSheet.Range["AD" + n, misValue].text;
                string Weight = activeSheet.Range["AE" + n, misValue].text;
                string Gender = activeSheet.Range["AF" + n, misValue].text;
                string InfectiousCondition = activeSheet.Range["AG" + n, misValue].text;
                string OrderingDoctor = activeSheet.Range["AH" + n, misValue].text;
                string PrimaryDoctor = activeSheet.Range["AI" + n, misValue].text;
                string DX1 = activeSheet.Range["AJ" + n, misValue].text;
                string DX2 = activeSheet.Range["AK" + n, misValue].text;
                string DX3 = activeSheet.Range["AL" + n, misValue].text;
                string DX4 = activeSheet.Range["AM" + n, misValue].text;
                string ICD10DX1 = activeSheet.Range["AN" + n, misValue].text;
                string ICD10DX2 = activeSheet.Range["AO" + n, misValue].text;
                string ICD10DX3 = activeSheet.Range["AP" + n, misValue].text;
                string ICD10DX4 = activeSheet.Range["AQ" + n, misValue].text;
                
                
                //Insurance Info
                //Primary
                string PrimInsID = activeSheet.Range["AR" + n, misValue].text;
                string PrimInsVerified = activeSheet.Range["AS" + n, misValue].text;
                string PrimInsPolicyNumber = activeSheet.Range["AT" + n, misValue].text;
                string PrimInsPayPct = activeSheet.Range["AU" + n, misValue].text;
                string PrimInsGroupNumber = activeSheet.Range["AV" + n, misValue].text;
                string PrimInsStartDate = activeSheet.Range["AW" + n, misValue].text;
                string PrimInsEndDate = activeSheet.Range["AX" + n, misValue].text;
                string PrimInsRelationship = activeSheet.Range["AY" + n, misValue].text;
                string PrimInsLastName = activeSheet.Range["AZ" + n, misValue].text;
                string PrimInsFirstName = activeSheet.Range["BA" + n, misValue].text;
                string PrimInsMidName = activeSheet.Range["BB" + n, misValue].text;
                string PrimInsSuffix = activeSheet.Range["BC" + n, misValue].text;
                string PrimInsDOB = activeSheet.Range["BD" + n, misValue].text;
                string PrimInsAddr1 = activeSheet.Range["BE" + n, misValue].text;
                string PrimInsAddr2 = activeSheet.Range["BF" + n, misValue].text;
                string PrimInsCity = activeSheet.Range["BG" + n, misValue].text;
                string PrimInsState = activeSheet.Range["BH" + n, misValue].text;
                string PrimInsPostal = activeSheet.Range["BI" + n, misValue].text;
                string PrimInsGender = activeSheet.Range["BJ" + n, misValue].text;

                //Secondary
                string SecInsID = activeSheet.Range["BK" + n, misValue].text;
                string SecInsVerified = activeSheet.Range["BL" + n, misValue].text;
                string SecInsPolicyNumber = activeSheet.Range["BM" + n, misValue].text;
                string SecInsPayPct = activeSheet.Range["BN" + n, misValue].text;
                string SecInsGroupNumber = activeSheet.Range["BO" + n, misValue].text;
                string SecInsStartDate = activeSheet.Range["BP" + n, misValue].text;
                string SecInsEndDate = activeSheet.Range["BQ" + n, misValue].text;
                string SecInsRelationship = activeSheet.Range["BR" + n, misValue].text;
                string SecInsLastName = activeSheet.Range["BS" + n, misValue].text;
                string SecInsFirstName = activeSheet.Range["BT" + n, misValue].text;
                string SecInsMidName = activeSheet.Range["BU" + n, misValue].text;
                string SecInsSuffix = activeSheet.Range["BV" + n, misValue].text;
                string SecInsDOB = activeSheet.Range["BW" + n, misValue].text;
                string SecInsAddr1 = activeSheet.Range["BX" + n, misValue].text;
                string SecInsAddr2 = activeSheet.Range["BY" + n, misValue].text;
                string SecInsCity = activeSheet.Range["BZ" + n, misValue].text;
                string SecInsState = activeSheet.Range["CA" + n, misValue].text;
                string SecInsPostal = activeSheet.Range["CB" + n, misValue].text;
                string SecInsGender = activeSheet.Range["CC" + n, misValue].text;

                //Tertiary
                string TerInsID = activeSheet.Range["CD" + n, misValue].text;
                string TerInsVerified = activeSheet.Range["CE" + n, misValue].text;
                string TerInsPolicyNumber = activeSheet.Range["CF" + n, misValue].text;
                string TerInsPayPct = activeSheet.Range["CG" + n, misValue].text;
                string TerInsGroupNumber = activeSheet.Range["CH" + n, misValue].text;
                string TerInsStartDate = activeSheet.Range["CI" + n, misValue].text;
                string TerInsEndDate = activeSheet.Range["CJ" + n, misValue].text;
                string TerInsRelationship = activeSheet.Range["CK" + n, misValue].text;
                string TerInsLastName = activeSheet.Range["CL" + n, misValue].text;
                string TerInsFirstName = activeSheet.Range["CM" + n, misValue].text;
                string TerInsMidName = activeSheet.Range["CN" + n, misValue].text;
                string TerInsSuffix = activeSheet.Range["CO" + n, misValue].text;
                string TerInsDOB = activeSheet.Range["CP" + n, misValue].text;
                string TerInsAddr1 = activeSheet.Range["CQ" + n, misValue].text;
                string TerInsAddr2 = activeSheet.Range["CR" + n, misValue].text;
                string TerInsCity = activeSheet.Range["CS" + n, misValue].text;
                string TerInsState = activeSheet.Range["CT" + n, misValue].text;
                string TerInsPostal = activeSheet.Range["CU" + n, misValue].text;
                string TerInsGender = activeSheet.Range["CV" + n, misValue].text;
                #endregion

                #region PatientCreation
                ptService.PatientServiceClient ptMethods = ServiceInitializers.CreatePatientOrderServiceClient();

                ptService.Patient ptRecord = new ptService.Patient();
                ptRecord.PatientGeneralInfo = new ptService.PatientGeneralInfo();
                
                ptRecord.PatientGeneralInfo.Branch = new ptService.LookupValue();
                ptRecord.PatientGeneralInfo.Name = new ptService.Name();
                ptRecord.PatientGeneralInfo.BillingAddress = new ptService.Address();
                ptRecord.PatientGeneralInfo.DeliveryAddress = new ptService.Address();
                ptRecord.PatientGeneralInfo.BillingContactInfo = new ptService.ContactInfo();
                ptRecord.PatientClinicalInfo = new ptService.PatientClinicalInfo();
                ptRecord.PatientGeneralInfo.TaxZone = new ptService.LookupValue();
                ptRecord.PatientClinicalInfo.OrderingDoctor = new ptService.DoctorInfo();

                ptRecord.PatientClinicalInfo.PrimaryDoctor = new ptService.DoctorInfo();
                ptRecord.PatientClinicalInfo.PrimaryDoctor.Doctor = new ptService.LookupValue();
                ptRecord.PatientClinicalInfo.OrderingDoctor.Doctor = new ptService.LookupValue();
                #endregion


                //need to count diagnoses to pass to array
                int countDX = 0;
                int count10DX = 0;

                ptService.ICDCodeInfo pt10Diag1 = new ptService.ICDCodeInfo();
                ptService.ICDCodeInfo pt10Diag2 = new ptService.ICDCodeInfo();
                ptService.ICDCodeInfo pt10Diag3 = new ptService.ICDCodeInfo();
                ptService.ICDCodeInfo pt10Diag4 = new ptService.ICDCodeInfo();
                
                ptService.ICDCodeInfo ptDiag1 = new ptService.ICDCodeInfo();
                ptService.ICDCodeInfo ptDiag2 = new ptService.ICDCodeInfo();
                ptService.ICDCodeInfo ptDiag3 = new ptService.ICDCodeInfo();
                ptService.ICDCodeInfo ptDiag4 = new ptService.ICDCodeInfo();                

                //account on hold
                bool acctHoldResult = new Boolean();
                if(AccountOnHold == "Y")               
                    acctHoldResult = true;
                else                
                    acctHoldResult = false;
                

                ptRecord.PatientGeneralInfo.Branch.ID = Convert.ToInt32(BranchOffice);
                ptRecord.PatientGeneralInfo.CustomerType = ptService.PatientCustomerType.Patient;

                ptRecord.PatientGeneralInfo.Name.Last = LastName;
                ptRecord.PatientGeneralInfo.Name.First = FirstName;
                ptRecord.PatientGeneralInfo.Name.Middle = MiddleName;
                ptRecord.PatientGeneralInfo.Name.Suffix = Suffix;
                ptRecord.PatientGeneralInfo.AccountNumber = AccountNumber;
                ptRecord.PatientGeneralInfo.AccountOnHold = acctHoldResult;
                ptRecord.PatientGeneralInfo.BillingAddress.AddressLine1 = BillingAddress1;
                ptRecord.PatientGeneralInfo.BillingAddress.AddressLine2 = BillingAddress2;
                ptRecord.PatientGeneralInfo.BillingAddress.City = City;
                ptRecord.PatientGeneralInfo.BillingAddress.State = State;
                ptRecord.PatientGeneralInfo.BillingAddress.PostalCode = PostalCode;
                ptRecord.PatientGeneralInfo.BillingContactInfo.PhoneNumber = BillingPhone;
                ptRecord.PatientGeneralInfo.BillingContactInfo.MobilePhoneNumber = MobilePhone;
                ptRecord.PatientGeneralInfo.BillingContactInfo.EmailAddress = Email;

                ptRecord.PatientGeneralInfo.DeliveryAddress.AddressLine1 = DelivAddress1;
                ptRecord.PatientGeneralInfo.DeliveryAddress.AddressLine2 = DelivAddress2;
                ptRecord.PatientGeneralInfo.DeliveryAddress.City = DelivCity;
                ptRecord.PatientGeneralInfo.DeliveryAddress.State = DelivState;
                ptRecord.PatientGeneralInfo.DeliveryAddress.PostalCode = DelivPostalCode;                

                ptRecord.PatientGeneralInfo.SSN = SSN;
                //If there is no Date of Birth.
                if(DOB.Length != 0)
                    ptRecord.PatientGeneralInfo.BirthDate = Convert.ToDateTime(DOB);

                ptRecord.PatientGeneralInfo.TaxZone.ID = Convert.ToInt32(TaxZone);
                ptRecord.PatientGeneralInfo.User1 = User1;
                ptRecord.PatientGeneralInfo.User2 = User2;
                ptRecord.PatientGeneralInfo.User3 = User3;
                ptRecord.PatientGeneralInfo.User4 = User4;

                if(Height != "")
                {
                    ptRecord.PatientClinicalInfo.Height = Convert.ToDecimal(Height);
                }
                if(Weight != "")
                {
                    ptRecord.PatientClinicalInfo.Weight = Convert.ToDecimal(Weight);
                }
                
                //Gender
                #region
                if (Gender == "M")                
                    ptRecord.PatientClinicalInfo.Gender = ptService.Gender.Male;
                
                else if (Gender == "F")                
                    ptRecord.PatientClinicalInfo.Gender = ptService.Gender.Female;
                
                else                
                    ptRecord.PatientClinicalInfo.Gender = ptService.Gender.NoneSpecified;
                
                #endregion

                //Infectious Condition
                #region
                if (InfectiousCondition == "Y")
                {
                    ptRecord.PatientClinicalInfo.InfectiousCondition = true;
                }
                else
                {
                    ptRecord.PatientClinicalInfo.InfectiousCondition = false;
                }
                #endregion
                if(OrderingDoctor !="")                
                    ptRecord.PatientClinicalInfo.OrderingDoctor.Doctor.ID = Convert.ToInt32(OrderingDoctor);                
                if(PrimaryDoctor !="")                
                    ptRecord.PatientClinicalInfo.PrimaryDoctor.Doctor.ID = Convert.ToInt32(PrimaryDoctor);                
                
                //ICD 9 diagnoses
                #region

                
                if (DX1.Length > 0)
                {
                    countDX = countDX + 1;
                    ptDiag1.ICDCode = DX1;
                    ptDiag1.DiagType = ptService.DiagType.ICD9;
                    ptDiag1.Sequence = 1;

                    if (DX2.Length > 0)
                    {
                        countDX = countDX + 1;
                        ptDiag2.ICDCode = DX2;
                        ptDiag2.DiagType = ptService.DiagType.ICD9;
                        ptDiag2.Sequence = 2;

                        if (DX3.Length > 0)
                        {
                            countDX = countDX + 1;
                            ptDiag3.ICDCode = DX3;
                            ptDiag3.DiagType = ptService.DiagType.ICD9;
                            ptDiag3.Sequence = 3;

                            if (DX4.Length > 0)
                            {
                                countDX = countDX + 1;
                                ptDiag4.ICDCode = DX4;
                                ptDiag4.DiagType = ptService.DiagType.ICD9;
                                ptDiag4.Sequence = 4;
                            }
                        }
                    }
                }
                List<ptService.ICDCodeInfo> ICDs = new List<ptService.ICDCodeInfo>();

                if(countDX == 4)
                {
                    ICDs.Add(ptDiag1);
                    ICDs.Add(ptDiag2);
                    ICDs.Add(ptDiag3);
                    ICDs.Add(ptDiag4);
                    
                }
                else if(countDX == 3)
                {
                    ICDs.Add(ptDiag1);
                    ICDs.Add(ptDiag2);
                    ICDs.Add(ptDiag3);
                }
                else if(countDX == 2)
                {
                    ICDs.Add(ptDiag1);
                    ICDs.Add(ptDiag2);
                    
                }
                else if(countDX == 1)
                {
                    ICDs.Add(ptDiag1);
                }

                ptRecord.PatientClinicalInfo.DiagnosisCodes = ICDs.ToArray();
#endregion

                //ICD 10 Diagnoses
                #region
                if (ICD10DX1.Length > 0)
                {
                    count10DX = count10DX + 1;
                    pt10Diag1.ICDCode = ICD10DX1;
                    pt10Diag1.DiagType = ptService.DiagType.ICD10;
                    pt10Diag1.Sequence = 1;

                    if (ICD10DX2.Length > 0)
                    {
                        count10DX = count10DX + 1;
                        pt10Diag2.ICDCode = ICD10DX2;
                        pt10Diag2.DiagType = ptService.DiagType.ICD10;
                        pt10Diag2.Sequence = 2;

                        if (ICD10DX3.Length > 0)
                        {
                            count10DX = count10DX + 1;
                            pt10Diag3.ICDCode = ICD10DX3;
                            pt10Diag3.DiagType = ptService.DiagType.ICD10;
                            pt10Diag3.Sequence = 3;

                            if (ICD10DX4.Length > 0)
                            {
                                count10DX = count10DX + 1;
                                pt10Diag4.ICDCode = ICD10DX4;
                                pt10Diag4.DiagType = ptService.DiagType.ICD10;
                                pt10Diag4.Sequence = 4;
                            }
                        }
                    }
                }
                List<ptService.ICDCodeInfo> ICD10s = new List<ptService.ICDCodeInfo>();

                if (count10DX == 4)
                {
                    ICD10s.Add(pt10Diag1);
                    ICD10s.Add(pt10Diag2);
                    ICD10s.Add(pt10Diag3);
                    ICD10s.Add(pt10Diag4);

                }
                else if (count10DX == 3)
                {
                    ICD10s.Add(pt10Diag1);
                    ICD10s.Add(pt10Diag2);
                    ICD10s.Add(pt10Diag3);
                }
                else if (count10DX == 2)
                {
                    ICD10s.Add(pt10Diag1);
                    ICD10s.Add(pt10Diag2);

                }
                else if (count10DX == 1)
                {
                    ICD10s.Add(pt10Diag1);
                }

                ptRecord.PatientClinicalInfo.DiagnosisCodes = ICD10s.ToArray();
                #endregion

                ptService.DataWriteServiceResponse ptCreateResponse = ptMethods.PatientCreate(ptRecord);
                  int? ptCreateID = new int();
                if(ptCreateResponse.Success == true)
                {
                    ptCreateID = ptCreateResponse.UpdatedDataKey;
                    activeSheet.Range["CW" + n, misValue].Value = "Success";
                    activeSheet.Range["CX" + n, misValue].Value = ptCreateID.ToString();
                }
                else
                {
                    activeSheet.Range["CW" + n, misValue].Value = ptCreateResponse.Messages[0];
                }                
               
                ptService.PatientPayor ptPayor1 = new ptService.PatientPayor();
                ptPayor1.Policy = new ptService.PatientPolicy();
                ptPayor1.Insured = new ptService.PatientInsured();
                ptPayor1.Insured.Name = new ptService.Name();
                ptPayor1.Insured.Address = new ptService.Address();
                ptPayor1.EligibilityInfo = new ptService.EligibilityVerification();
               
                ptService.PatientPayor ptPayor2 = new ptService.PatientPayor();
                ptPayor2.Policy = new ptService.PatientPolicy();
                ptPayor2.Insured = new ptService.PatientInsured();
                ptPayor2.Insured.Name = new ptService.Name();
                ptPayor2.Insured.Address = new ptService.Address();
                ptPayor2.EligibilityInfo = new ptService.EligibilityVerification();
               
                ptService.PatientPayor ptPayor3 = new ptService.PatientPayor();
                ptPayor3.Policy = new ptService.PatientPolicy();
                ptPayor3.Insured = new ptService.PatientInsured();
                ptPayor3.Insured.Name = new ptService.Name();
                ptPayor3.Insured.Address = new ptService.Address();
                ptPayor3.EligibilityInfo = new ptService.EligibilityVerification();
               

                if(PrimInsID.Length > 0)
                {
                    //primary Ins
                    ptPayor1.payorLevel = ptService.PayorLevel.Primary;
                    ptPayor1.BrightreeID = Convert.ToInt32(PrimInsID);

                    //verification
                    if (PrimInsVerified == "Y") { ptPayor1.EligibilityInfo.EligibilityVerified = true; }
                    else { ptPayor1.EligibilityInfo.EligibilityVerified = false; }



                    ptPayor1.Policy.PolicyNumber = PrimInsPolicyNumber;
                    ptPayor1.Policy.PayPercent = Convert.ToDecimal(PrimInsPayPct);
                    ptPayor1.Policy.GroupNumber = PrimInsGroupNumber;
                    ptPayor1.Policy.StartDate = Convert.ToDateTime(PrimInsStartDate);
                    if(PrimInsEndDate.Length > 0)                    
                        ptPayor1.Policy.EndDate = Convert.ToDateTime(PrimInsEndDate);
                    
    
                    //relationship
                    #region
                    if (PrimInsRelationship == "Self")
                    {
                        ptPayor1.Policy.Relationship = ptService.RelationshipToInsured.Self;
                    }
                    else if (PrimInsRelationship == "Child")
                    {
                        ptPayor1.Policy.Relationship = ptService.RelationshipToInsured.Child;
                    }
                    else if (PrimInsRelationship == "Spouse")
                    {
                        ptPayor1.Policy.Relationship = ptService.RelationshipToInsured.Spouse;
                    }
                    else
                    {
                        ptPayor1.Policy.Relationship = ptService.RelationshipToInsured.Other;
                    }
                    #endregion

                    if (PrimInsRelationship != "Self")
                    {
                        ptPayor1.Insured.Name.First = PrimInsFirstName;
                        ptPayor1.Insured.Name.Middle = PrimInsMidName;
                        ptPayor1.Insured.Name.Suffix = PrimInsSuffix;
                        ptPayor1.Insured.BirthDate = Convert.ToDateTime(PrimInsDOB);
                        ptPayor1.Insured.Address.AddressLine1 = PrimInsAddr1;
                        ptPayor1.Insured.Address.AddressLine2 = PrimInsAddr2;
                        ptPayor1.Insured.Address.City = PrimInsCity;
                        ptPayor1.Insured.Address.State = PrimInsState;
                        ptPayor1.Insured.Address.PostalCode = PrimInsPostal;
                        //gender
                        if (PrimInsGender == "M") 
                            ptPayor1.Insured.Gender = ptService.Gender.Male; 
                        else if (PrimInsGender == "F") 
                            ptPayor1.Insured.Gender = ptService.Gender.Female; 
                        else
                             ptPayor1.Insured.Gender = ptService.Gender.NoneSpecified; 
                        }

                        //add the payor to the patient
                        ptService.DataWriteServiceResponse ptPayor1Resp = ptMethods.PatientPayorAdd(Convert.ToInt32(ptCreateID), Convert.ToInt32(PrimInsID), ptPayor1);
        
                        if(ptPayor1Resp.Success == true)                        
                            activeSheet.Range["CY" + n, misValue].Value = "Success";                        
                        else                        
                            activeSheet.Range["CY" + n, misValue].Value = ptPayor1Resp.Messages[0];                           
                    }
                    //If primary is Self
                    else if(ptPayor1.Policy.Relationship == ptService.RelationshipToInsured.Self)
                    {
                        ptPayor1.Insured.BirthDate = Convert.ToDateTime(PrimInsDOB);
                        //gender
                        if (PrimInsGender == "M")
                            ptPayor1.Insured.Gender = ptService.Gender.Male;
                        else if (PrimInsGender == "F")
                            ptPayor1.Insured.Gender = ptService.Gender.Female;
                        else
                            ptPayor1.Insured.Gender = ptService.Gender.NoneSpecified; 

                    }
                    if (SecInsID.Length > 0)
                    {
                        //primary Ins
                        ptPayor2.payorLevel = ptService.PayorLevel.Secondary;
                        ptPayor2.BrightreeID = Convert.ToInt32(SecInsID);

                        //verification
                        if (SecInsVerified == "Y")  
                            ptPayor2.EligibilityInfo.EligibilityVerified = true; 
                        else 
                            ptPayor2.EligibilityInfo.EligibilityVerified = false; 

                        ptPayor2.Policy.PolicyNumber = SecInsPolicyNumber;
                        ptPayor2.Policy.PayPercent = Convert.ToDecimal(SecInsPayPct);
                        ptPayor2.Policy.GroupNumber = SecInsGroupNumber;
                        ptPayor2.Policy.StartDate = Convert.ToDateTime(SecInsStartDate);
                        if (SecInsEndDate.Length > 0)                        
                            ptPayor2.Policy.EndDate = Convert.ToDateTime(SecInsEndDate);                        

                        //relationship
                        #region
                        if (SecInsRelationship == "Self")
                        {
                            ptPayor2.Policy.Relationship = ptService.RelationshipToInsured.Self;
                        }
                        else if (SecInsRelationship == "Child")
                        {
                            ptPayor2.Policy.Relationship = ptService.RelationshipToInsured.Child;
                        }
                        else if (SecInsRelationship == "Spouse")
                        {
                            ptPayor2.Policy.Relationship = ptService.RelationshipToInsured.Spouse;
                        }
                        else
                        {
                            ptPayor2.Policy.Relationship = ptService.RelationshipToInsured.Other;
                        }
                        #endregion

                        if (SecInsRelationship != "Self")
                        {
                            ptPayor2.Insured.Name.First = SecInsFirstName;
                            ptPayor2.Insured.Name.Middle = SecInsMidName;
                            ptPayor2.Insured.Name.Suffix = SecInsSuffix;
                            if(SecInsDOB != "")
                                ptPayor2.Insured.BirthDate = Convert.ToDateTime(SecInsDOB);
                            ptPayor2.Insured.Address.AddressLine1 = SecInsAddr1;
                            ptPayor2.Insured.Address.AddressLine2 = SecInsAddr2;
                            ptPayor2.Insured.Address.City = SecInsCity;
                            ptPayor2.Insured.Address.State = SecInsState;
                            ptPayor2.Insured.Address.PostalCode = SecInsPostal;
                            //gender
                            if (SecInsGender == "M") 
                                ptPayor2.Insured.Gender = ptService.Gender.Male; 
                            else if (SecInsGender == "F")  
                                ptPayor2.Insured.Gender = ptService.Gender.Female; 
                            else  
                                ptPayor2.Insured.Gender = ptService.Gender.NoneSpecified; 
                        }

                        //add the payor to the patient
                        ptService.DataWriteServiceResponse ptPayor2Resp = ptMethods.PatientPayorAdd(Convert.ToInt32(ptCreateID), Convert.ToInt32(SecInsID), ptPayor2);

                        if (ptPayor2Resp.Success == true)
                        {
                            activeSheet.Range["CZ" + n, misValue].Value = "Success";
                        }
                        else
                        {
                            activeSheet.Range["CZ" + n, misValue].Value = ptPayor2Resp.Messages[0];
                            goto patientAddFailCutoff;
                        }
                     }

                    if (TerInsID.Length > 0)
                    {
                        //primary Ins
                        ptPayor3.payorLevel = ptService.PayorLevel.Primary;
                        ptPayor3.BrightreeID = Convert.ToInt32(TerInsID);

                        //verification
                        if (TerInsVerified == "Y") 
                            ptPayor3.EligibilityInfo.EligibilityVerified = true; 
                        else  
                            ptPayor3.EligibilityInfo.EligibilityVerified = false;                

                        ptPayor3.Policy.PolicyNumber = TerInsPolicyNumber;
                        ptPayor3.Policy.PayPercent = Convert.ToDecimal(TerInsPayPct);
                        ptPayor3.Policy.GroupNumber = TerInsGroupNumber;
                        ptPayor3.Policy.StartDate = Convert.ToDateTime(TerInsStartDate);
                        if (TerInsEndDate.Length > 0)                
                            ptPayor3.Policy.EndDate = Convert.ToDateTime(TerInsEndDate);
                

                        //relationship
                        #region
                        if (TerInsRelationship == "Self")
                        {

                            ptPayor3.Policy.Relationship = ptService.RelationshipToInsured.Self;
                        }
                        else if (TerInsRelationship == "Child")
                        {
                            ptPayor3.Policy.Relationship = ptService.RelationshipToInsured.Child;
                        }
                        else if (TerInsRelationship == "Spouse")
                        {
                            ptPayor3.Policy.Relationship = ptService.RelationshipToInsured.Spouse;
                        }
                        else
                        {
                            ptPayor3.Policy.Relationship = ptService.RelationshipToInsured.Other;
                        }
                        #endregion
                        if (TerInsRelationship != "Self")
                        {

                            ptPayor3.Insured.Name.First = TerInsFirstName;
                            ptPayor3.Insured.Name.Middle = TerInsMidName;
                            ptPayor3.Insured.Name.Suffix = TerInsSuffix;
                            ptPayor3.Insured.BirthDate = Convert.ToDateTime(TerInsDOB);
                            ptPayor3.Insured.Address.AddressLine1 = TerInsAddr1;
                            ptPayor3.Insured.Address.AddressLine2 = TerInsAddr2;
                            ptPayor3.Insured.Address.City = TerInsCity;
                            ptPayor3.Insured.Address.State = TerInsState;
                            ptPayor3.Insured.Address.PostalCode = TerInsPostal;
                            //gender
                            if (TerInsGender == "M") 
                                ptPayor3.Insured.Gender = ptService.Gender.Male; 
                            else if (TerInsGender == "F")  
                                ptPayor3.Insured.Gender = ptService.Gender.Female; 
                            else  
                                ptPayor3.Insured.Gender = ptService.Gender.NoneSpecified; 
                    }

                    //add the payor to the patient
                    ptService.DataWriteServiceResponse ptPayor3Resp = ptMethods.PatientPayorAdd(Convert.ToInt32(ptCreateID), Convert.ToInt32(TerInsID), ptPayor3);

                    if (ptPayor3Resp.Success == true)
                    {
                        activeSheet.Range["DA" + n, misValue].Value = "Success";
                    }
                    else
                    {
                        activeSheet.Range["DA" + n, misValue].Value = ptPayor3Resp.Messages[0];
                    }
                }
                patientAddFailCutoff:
                //Progress n to the next row
                    n = n + 1;
            }
            MessageBox.Show("Operation Complete");
        }

        private void RunButton_Click(object sender, EventArgs e)
        {
            backgroundWorker1.RunWorkerAsync();
        }

        private void CancelButton_Click(object sender, EventArgs e)
        {
            backgroundWorker1.CancelAsync();
        }

    }
}
