﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ServiceModel;

namespace PatientConversionTool
{
    class ServiceInitializers
    {
        public static SOService.SalesOrderServiceClient CreateSalesOrderServiceClient()
        {
            string UserID = Globals.Sheet1.UserName.Text.ToString();
            string UserPassword = Globals.Sheet1.Password.Text.ToString();

            BasicHttpBinding myBinding = new BasicHttpBinding();
            myBinding.Security.Mode = BasicHttpSecurityMode.Transport;
            myBinding.Security.Transport.ClientCredentialType = HttpClientCredentialType.Basic;
            EndpointAddress ea = new EndpointAddress("https://webservices.brightree.net/v0100-1307/OrderEntryService/SalesOrderService.svc");
            SOService.SalesOrderServiceClient client = new SOService.SalesOrderServiceClient();
            client.ClientCredentials.UserName.UserName = UserID;
            client.ClientCredentials.UserName.Password = UserPassword;
            return client;


        }
        public static ptService.PatientServiceClient CreatePatientOrderServiceClient()
        {
            string UserID = Globals.Sheet1.UserName.Text.ToString();
            string UserPassword = Globals.Sheet1.Password.Text.ToString();

            BasicHttpBinding myBinding = new BasicHttpBinding();
            myBinding.Security.Mode = BasicHttpSecurityMode.Transport;
            myBinding.Security.Transport.ClientCredentialType = HttpClientCredentialType.Basic;
            EndpointAddress ea = new EndpointAddress("https://webservices.brightree.net/v0100-1505/OrderEntryService/patientservice.svc");
            ptService.PatientServiceClient client = new ptService.PatientServiceClient();
            client.ClientCredentials.UserName.UserName = UserID;
            client.ClientCredentials.UserName.Password = UserPassword;
            return client;


        }
        public static refService.ReferenceDataServiceClient CreateReferenceServiceClient()
        {
            string UserID = Globals.Sheet1.UserName.Text.ToString();
            string UserPassword = Globals.Sheet1.Password.Text.ToString();

            BasicHttpBinding myBinding = new BasicHttpBinding();
            myBinding.Security.Mode = BasicHttpSecurityMode.Transport;
            myBinding.Security.Transport.ClientCredentialType = HttpClientCredentialType.Basic;
            EndpointAddress ea = new EndpointAddress("https://webservices.brightree.net/v0100-1505/ReferenceDataService/ReferenceDataService.svc");
            refService.ReferenceDataServiceClient client = new refService.ReferenceDataServiceClient();
            client.ClientCredentials.UserName.UserName = UserID;
            client.ClientCredentials.UserName.Password = UserPassword;
            return client;


        }



    }
}
